package com.zyw.gulimall.coupon.dao;

import com.zyw.gulimall.coupon.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-03 13:00:11
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
