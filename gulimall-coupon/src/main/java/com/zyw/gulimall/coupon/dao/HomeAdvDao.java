package com.zyw.gulimall.coupon.dao;

import com.zyw.gulimall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-03 13:00:11
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
