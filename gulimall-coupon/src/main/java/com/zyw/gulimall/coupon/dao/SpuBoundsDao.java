package com.zyw.gulimall.coupon.dao;

import com.zyw.gulimall.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 23:52:22
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
