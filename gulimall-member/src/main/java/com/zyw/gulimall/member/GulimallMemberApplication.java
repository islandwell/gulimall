package com.zyw.gulimall.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 想要调用远程服务
 * 1.引用openfeign
 * 2.编写一个接口，告诉springcloud需要调用一个接口s
 * 3.将签名复制到相关的接口方法中去
 * 在application中加入@EnableFeignClients(basePackages = "com.zyw.gulimall.member.feign")注解
 */
@EnableFeignClients(basePackages = "com.zyw.gulimall.member.feign")
@MapperScan("com.zyw.gulimall.member.dao")
@SpringBootApplication
@EnableDiscoveryClient
public class GulimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallMemberApplication.class, args);
    }

}
