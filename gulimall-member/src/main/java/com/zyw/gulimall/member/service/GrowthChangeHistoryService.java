package com.zyw.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.gulimall.common.utils.PageUtils;
import com.zyw.gulimall.member.entity.GrowthChangeHistoryEntity;

import java.util.Map;

/**
 * 成长值变化历史记录
 *
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 23:56:48
 */
public interface GrowthChangeHistoryService extends IService<GrowthChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

