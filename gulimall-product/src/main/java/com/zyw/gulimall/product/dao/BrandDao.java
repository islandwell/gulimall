package com.zyw.gulimall.product.dao;

import com.zyw.gulimall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 23:01:44
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
