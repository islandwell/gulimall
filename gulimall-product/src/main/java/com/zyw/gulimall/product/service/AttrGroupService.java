package com.zyw.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.gulimall.common.utils.PageUtils;
import com.zyw.gulimall.product.entity.AttrGroupEntity;

import java.util.Map;

/**
 * 属性分组
 *
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 23:01:44
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

