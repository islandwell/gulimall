package com.zyw.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.gulimall.common.utils.PageUtils;
import com.zyw.gulimall.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 23:01:44
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

