package com.zyw.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.gulimall.common.utils.PageUtils;
import com.zyw.gulimall.product.entity.SkuSaleAttrValueEntity;

import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 22:41:32
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

