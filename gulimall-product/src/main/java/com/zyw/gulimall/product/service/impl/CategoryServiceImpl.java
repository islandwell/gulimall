package com.zyw.gulimall.product.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.gulimall.common.utils.PageUtils;
import com.zyw.gulimall.common.utils.Query;

import com.zyw.gulimall.product.dao.CategoryDao;
import com.zyw.gulimall.product.entity.CategoryEntity;
import com.zyw.gulimall.product.service.CategoryService;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1 查出所有分类
        List<CategoryEntity> categoryEntities = this.baseMapper.selectList(null);
        //2 组装成所有的父子结构
        //2.1 找到所有的一级分类
        List<CategoryEntity> collect = categoryEntities.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == 0;
        }).map((menu)->{
            menu.setChildren(getChildrens(menu,categoryEntities));
            return menu;
        }).sorted((menu1,menu2)->{
            try {
                return menu1.getSort() - menu2.getSort();
            }catch (Exception e){
                return 0;
            }
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 找后续的菜单
     * @param root
     * @param all
     * @return
     */
    private  List<CategoryEntity> getChildrens(CategoryEntity root,List<CategoryEntity> all){
        List<CategoryEntity> collect = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().equals(root.getCatId());
        }).map(categoryEntity -> {
            categoryEntity.setChildren(getChildrens(categoryEntity,all));
            return categoryEntity;
        }).sorted((menu1,menu2)->{
            try {
                return menu1.getSort() - menu2.getSort();
            }catch (Exception e){
                return 0;
            }
        }).collect(Collectors.toList());
        return collect;
    }

}