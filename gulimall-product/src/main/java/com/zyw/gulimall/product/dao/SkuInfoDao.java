package com.zyw.gulimall.product.dao;

import com.zyw.gulimall.product.entity.SkuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * sku信息
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-02 22:41:32
 */
@Mapper
public interface SkuInfoDao extends BaseMapper<SkuInfoEntity> {
	
}
