package com.zyw.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.gulimall.common.utils.PageUtils;
import com.zyw.gulimall.order.entity.OrderReturnReasonEntity;

import java.util.Map;

/**
 * 退货原因
 *
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-03 00:05:01
 */
public interface OrderReturnReasonService extends IService<OrderReturnReasonEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

