package com.zyw.gulimall.ware.dao;

import com.zyw.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-03 00:09:58
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
