package com.zyw.gulimall.ware.dao;

import com.zyw.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author zhuyaowei
 * @email 2743282637@qq.com
 * @date 2021-05-03 00:09:58
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
